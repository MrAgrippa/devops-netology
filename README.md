# Домашнее задание к занятию «Системы контроля версий» - Тимохин Максим

В файле .gitignore будут игнорироваться:

  1. все файлы в локальной директории **/.terraform/*
  2. файлы содержащие в имени *.tfstate, *.tfstate.*
  3. файлы ошибок(логов) crash.log, crash.*.log
  4. файлы которые могут содержать приватную информацию *.tfvars, *.tfvars.json
  5. файлы переопределения override.tf, override.tf.json, *_override.tf, *_override.tf.json
  6. файлы CLI конфигурации .terraformrc, terraform.rc
